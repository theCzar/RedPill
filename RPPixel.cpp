/*----------------------------------------------------------------------------
  This file is part of the RedPill library.

  Licence: MIT/X11 (see LICENCE file)
  Author: 0x783czar (Diego Carrión)
  ----------------------------------------------------------------------------*/

#include "RPPixel.h"

RPPixel::RPPixel(uint8_t x, uint8_t y, uint16_t serial, Adafruit_NeoPixel* strip)
{
  this->xPosition = x;
  this->yPosition = y;
  this->serialNumber = serial;
  this->strip = strip;
}

RPPixel::~RPPixel()
{
  // deconstructor
}

void RPPixel::setColor(uint32_t color)
{
  this->strip->setPixelColor(this->serialNumber, color);
}

void RPPixel::setColor(uint8_t r, uint8_t g, uint8_t b)
{
  this->strip->setPixelColor(this->serialNumber, r, g, b);
}

void RPPixel::off()
{
  this->strip->setPixelColor(this->serialNumber, 0);
}

uint8_t RPPixel::getXPosition()
{
  return this->xPosition;
}

uint8_t RPPixel::getYPosition()
{
  return this->yPosition;
}

uint16_t RPPixel::getSerialNumber()
{
  return this->serialNumber;
}

