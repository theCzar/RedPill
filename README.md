#RedPill
---

Object Oriented Library for controlling RGB LED Matrices, which provides an abstraction layer on top of Adafruit Industries' NeoPixel RGB LED Library.

###Features:

- 2-dimentional representation of the LEDs in the matrix
- Discrete objects for every pixel
- Nearly seamless syntax compatibility with Adafruit's NeoPixel library

###Example:

```cpp
// #include <Adafruit_NeoPixel.h> // depending on your library organization this may be necessary use RedPill
#include <RedPill.h>

RedPill matrix = RedPill(16, 16);

void setup()
{
  matrix.setBrightness(20); // will default to 15, can go as high as 255

  matrix.on();

  matrix.setPixel(0, 0, WHITE);
  matrix.setPixel(0, 1, RED);
  matrix.setPixel(0, 2, ORANGE);
  matrix.setPixel(0, 3, YELLOW);
  matrix.setPixel(0, 4, GREEN);
  matrix.setPixel(0, 5, BLUE);
  matrix.setPixel(0, 6, INDIGO);
  matrix.setPixel(0, 7, VIOLET);

  matrix.paint();
}
```

###Installation

1. Download latest source code tag
2. Decompress
3. Import/move into your Arduino libraries folder

###ToDo:

- Switch x axis and y axis (so that x is horizontal and y is vertical)
- Add the ability to drive multiple clustered matrices as one
- Write getters for the matrix and individual pixel objects to allow direct manipulation
- Add Examples, ideally based off of the Adafruit_NeoPixel librarie's 'strandtest' example
- Possibly allow for switching to a true Cartesian positive quadrant and set (0,0) in the lower left corner

